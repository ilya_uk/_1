﻿CREATE TABLE [dbo].[DATA_Abonent]
(
	[Id_Abonent] INT NOT NULL PRIMARY KEY, 
    [Name_Abonent] NVARCHAR(50) NOT NULL, 
    [Surname_Abonent] NVARCHAR(50) NOT NULL, 
    [Tarif] INT NOT NULL, 
    [Date_Registration] DATETIME NOT NULL, 
    [Period_Dogovor] TEXT NULL
)
